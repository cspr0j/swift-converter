package swift;

import com.prowidesoftware.swift.model.mt.mt1xx.MT103;

import java.io.File;
import java.io.IOException;

public class MtReader {

    public static MT103 getMessage(String filePath) {
        try {
            return MT103.parse(new File(filePath));
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }
}
