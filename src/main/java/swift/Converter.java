package swift;

import com.prowidesoftware.swift.model.SwiftBlock2Output;
import com.prowidesoftware.swift.model.field.StructuredNarrative;
import com.prowidesoftware.swift.model.mt.mt1xx.MT103;
import com.prowidesoftware.swift.model.mx.BusinessAppHdrV02;
import com.prowidesoftware.swift.model.mx.MxPacs00800108;
import com.prowidesoftware.swift.model.mx.dic.*;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class Converter {

    public static void main(String[] args) {
        // TODO add your own file
        var mtMessage = MtReader.getMessage("");
        convertToMX(mtMessage);
    }

    private static void convertToMX(MT103 mtMessage) {
        var mxMessage = new MxPacs00800108();
        mxMessage.setAppHdr(getAppHdr(mtMessage));
        mxMessage.setFIToFICstmrCdtTrf(getFiToFICstmrCdtTrf(mtMessage));

        // print in xml format
        System.out.println(mxMessage.document());
    }

    private static FIToFICustomerCreditTransferV08 getFiToFICstmrCdtTrf(MT103 mtMessage) {
        var creditTransfer = new FIToFICustomerCreditTransferV08();
        try {
            var groupHeader = new GroupHeader93();
            var block2 = (SwiftBlock2Output) mtMessage.getSwiftMessage().getBlock2();

            groupHeader.setMsgId(mtMessage.getField20().getReference());
            groupHeader.setCreDtTm(parse(block2));

            groupHeader.setSttlmInf(new SettlementInstruction7().setInstdRmbrsmntAgt(
                    new BranchAndFinancialInstitutionIdentification6().setFinInstnId(
                            new FinancialInstitutionIdentification18()
                                    .setBICFI(mtMessage.getField54A().getValue()))));

            creditTransfer.setGrpHdr(groupHeader);
        } catch (ParseException | DatatypeConfigurationException e) {
            throw new RuntimeException(e);
        }

        creditTransfer.addCdtTrfTxInf(getCdtTrfTxInf(mtMessage));
        return creditTransfer;
    }

    private static CreditTransferTransaction39 getCdtTrfTxInf(MT103 mtMessage) {
        var transferTransaction = new CreditTransferTransaction39();

        var narrative = mtMessage.getField70()
                .narrative();

        var structured = narrative
                .getStructured()
                .stream()
                .map(StructuredNarrative::getNarrative)
                .findFirst()
                .orElseThrow();

        transferTransaction
                .setDbtr(new PartyIdentification135()
                        .setNm(mtMessage.getField50F().getNameAndAddress1())
                        .setPstlAdr(new PostalAddress24()
                                .addAdrLine(mtMessage.getField50F().getNameAndAddress2())
                                .addAdrLine(mtMessage.getField50F().getNameAndAddress3())))
                .setPmtId(new PaymentIdentification7()
                        .setInstrId(mtMessage.getField20().getReference())
                        .setUETR(mtMessage.getUETR())
                        .setEndToEndId(structured.substring(0, structured.indexOf("/"))))
                .setRmtInf(new RemittanceInformation16().addUstrd(narrative.getUnstructured()));

        return transferTransaction;
    }

    private static BusinessAppHdrV02 getAppHdr(MT103 mtMessage) {
        var appHdr = new BusinessAppHdrV02();
        try {
            var block2 = (SwiftBlock2Output) mtMessage.getSwiftMessage().getBlock2();

            appHdr.setBizMsgIdr(mtMessage.getField20().getReference());
            appHdr.setCreDt(parse(block2));

            appHdr.setFr(new Party44Choice()
                    .setFIId(new BranchAndFinancialInstitutionIdentification6()
                            .setFinInstnId(new FinancialInstitutionIdentification18()
                                    .setBICFI(block2.getMIRLogicalTerminal()))));

            appHdr.setTo(new Party44Choice()
                    .setFIId(new BranchAndFinancialInstitutionIdentification6()
                            .setFinInstnId(new FinancialInstitutionIdentification18()
                                    .setBICFI(mtMessage.getLogicalTerminal()))));

        } catch (ParseException | DatatypeConfigurationException e) {
            throw new RuntimeException(e);
        }
        return appHdr;
    }

    private static XMLGregorianCalendar parse(SwiftBlock2Output block2) throws ParseException, DatatypeConfigurationException {
        SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyMMddHHmm");
        Date parsedDate = inputDateFormat.parse(block2.getReceiverOutputDate() + block2.getReceiverOutputTime());

        GregorianCalendar gc = new java.util.GregorianCalendar();
        gc.setTime(parsedDate);

        XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);

        System.out.println("XMLGregorianCalendar: " + xmlGregorianCalendar);
        return xmlGregorianCalendar;
    }
}
