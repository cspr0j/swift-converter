package swift;

import com.prowidesoftware.swift.model.mx.MxPacs00800108;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MxReader {
    public static void main(String[] args) {
        // TODO add your own file
        final String filePath = "";
        try {
            String content = readFileAsString(filePath);

            var mx = MxPacs00800108.parse(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String readFileAsString(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        return new String(Files.readAllBytes(path));
    }
}
